import React from 'react'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import App from './App';
import Dashboard from './components/Dashboard';
import Login from './components/Login';

const Main = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/dashboard" component={Dashboard} />
                <Redirect to="/" />
            </Switch>
        </Router>
    )
}

export default Main;