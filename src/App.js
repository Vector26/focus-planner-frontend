import React from 'react';
import './App.css';
import {useHistory} from 'react-router-dom';

function App() {

  const history = useHistory();

  const handleClick = () => {
    history.push('/login');
  }

  return (
    <div className="app">
      <div className="app__background">
        <div className="app__bgGradient"/>
      </div>
      <div className="app__main">
        <h1>Welcome to EasyPlanner!</h1>
        <p>Helps you plan and work on your important tasks</p>
        <div className="btn" onClick={handleClick}>Login</div>
      </div>
    </div>
  );
}

export default App;
