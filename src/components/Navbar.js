import React from 'react'
import './Navbar.css';

const Sidebar = () => {
    return (
        <div className="navbar">
            <h2>Easy Day Planner</h2>
            <ul>
                <li>To-Do</li>
                <li>Completed</li>
                <li>Logout</li>
            </ul>
        </div>
    )
}

export default Sidebar
