import React from 'react'
import './ErrorAlert.css'

const ErrorAlert = ({msg}) => {
    return (
        <div className="alert">
            <span className="closebtn" onClick={e => e.target.parentElement.style.display = "none"}>&times;</span> 
            <strong>{msg}</strong>
        </div>
    )
}

export default ErrorAlert
