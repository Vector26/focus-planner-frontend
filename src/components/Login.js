import React, { useState } from 'react'
import './Login.css';
import {Link, Redirect} from 'react-router-dom';
import loginImg from '../images/login.png';
import axios from '../util/axios';
import ErrorAlert from './ErrorAlert';

const Login = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);
    const [errorAlert, setErrorAlert] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const handleUsernameChange = (e) => {
        setUsername(e.target.value);
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        axios
			.post('user/login', { username: username, password: password })
			.then(response => {
                localStorage.setItem('username', username);
				localStorage.setItem('token', response.data.token);
				setRedirect(true);
			})
			.catch(err => {
				let temp_errormsg;
				if (err.response.status === 400) temp_errormsg = 'Username or password are incorrect.';
				else if (err.response.status === 403) temp_errormsg = 'Username and password fields can not be empty.';
				else if (err.response.status === 404) temp_errormsg = 'User not found.';
				else temp_errormsg = 'Something went wrong.';
                setErrorAlert(true);
                setErrorMsg(temp_errormsg);
			});
    }

    if (redirect) return <Redirect to="/dashboard" />;

    return (
        <div className="login">
            <div className="login__container">
                <div className="container__left">
                <div className="form-container sign-in-container">
                    <form action="#" onSubmit={handleSubmit}>
                        <h1>Login</h1>
                        <input type="text" placeholder="Username" onChange={handleUsernameChange} value={username} />
                        <input type="password" placeholder="Password" onChange={handlePasswordChange} value={password} />
                        <button>Sign In</button>
                        <Link to="/signup">Don't have an account? Register now.</Link>

                        {errorAlert ? (
                            <ErrorAlert msg={errorMsg}/>
                        ) : null}
                    </form>
                </div>
                </div>
                <div className="container__right">
                    <img src={loginImg} alt="" />
                </div>
            </div>
        </div>
    )
}

export default Login
