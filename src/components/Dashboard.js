import React, { useState, useEffect } from 'react'
import './Dashboard.css';
import Navbar from './Navbar';
import axios from '../util/axios';

const Dashboard = () => {

    const [tasks, setTasks] = useState([]);
    const [date, setDate] = useState(new Date(new Date().setDate(new Date().getDate())).toString().split(' ').splice(0, 4).join(' '))

    useEffect(() => {
        getAllTasks();
    }, [])

    const getAllTasks = () => {
        axios.get(`task/get`, { params: { loggedUser: localStorage.getItem('username') } }).then(res => {
			const usertasks = res.data;
			usertasks.reverse(); // For that : Users should be see task on top whichever is new
			setTasks(usertasks);
		});
    }

    return (
        <div className="dashboard">
            <Navbar className="dashboard__sidebar"/>
            <div className="dashboard__body">

                <h2 className="today_date">{date}</h2>

                <div className="taks_list">
                    {tasks.map(task => {
                        return (
                            <div className="task_single">
                                {task.title}
                            </div>
                        )
                    })}
                </div>

                <div className="addTask">
                    
                </div>

                <div className="btn__newTask">
                    Add new task
                </div>
            </div>
        </div>
    )
}

export default Dashboard
